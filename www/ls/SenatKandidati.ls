class ig.SenatKandidati
  ->
    @assoc = {}
    @array = []
    for line in d3.tsv.parse ig.data.kandidatiSenat
      id = @getId line.OBVOD, line.CKAND
      line.name = "#{line.JMENO} #{line.PRIJMENI}"
      line.party = line.NAZEV_VS
      line.number = parseInt line.CKAND, 10
      line.district = line.obvod = parseInt line.OBVOD, 10
      line.partyObject = ig.stranyUid[line.VSTRANA]
      line.partyShort = ig.stranyUid[line.VSTRANA]?zkratka
      line.zkratka = ig.stranyUid[line.partyColorId]?zkratka
      partyColor = ig.stranyUid[line.partyColorId]
      line.partyColor = ig.utils.getStranaColor (partyColor || line.partyObject || line.VSTRANA)
      line.partyColorStatic = partyColor?barva || line.partyObject?barva
      @assoc[id] = line
      @array.push line

  find: (obvod, kand) ->
    return @assoc[@getId obvod, kand]

  getId: (obvod, kand) ->
    "#obvod-#kand"
