maxInt = 65500
statsFields = <[okrsky_celkem okrsky_zprac zapsani_volici vydane_obalky platne_hlasy]>
obecTransform =
  "555134": [555096 555100 555118 555126 557064 557072 574716 575020] #pardubice
  "546046": [554537 546046 554227 546135 554685 546224] #ostrava
  "546224": [546224] #poruba
  "546135": [546135] # ostrava-jih


binaryString =
  decode: (str, position = 0) ->
    int = 0
    do
      decodedInt = str.charCodeAt position
      decodedInt -= 33
      if decodedInt < 0
        return null
      int += decodedInt
      position++
    while decodedInt == maxInt
    int

  decodeArray: (str, position = 0) ->
    lastPosition = str.length - 1
    out = while position <= lastPosition
      int = this.decode str, position
      position += 1 + Math.floor (int || 0) / maxInt
      int
    out

class ig.KrajeResultDecoder
  decode: (str) ->
    [strany, ...obce] = str.split "\n"
    stranyIds = for i in [0 til strany.length]
      binaryString.decode strany[i]
    allObce = []
    for obec in obce
      obecId = parseInt do
        obec.substr 0, 6
        10
      values = binaryString.decodeArray obec, 6
      stats = {}
      for field, index in statsFields
        stats[field] = values[index]
      values .= slice statsFields.length
      stranyAssoc = {}
      strany = []
      for stranaId, index in stranyIds
        id = stranaId
        hlasy = values[index]
        continue if hlasy is null
        stranyAssoc[id] = hlasy
        strana = {id, hlasy}
        strany.push strana
      if obecTransform[obecId]
        for newId in obecTransform[obecId]
          allObce.push {id:newId, stats, strany, stranyAssoc}
      allObce.push {id:obecId, stats, strany, stranyAssoc}
    allObce
