resultsDecoder = new ig.KrajeResultDecoder
class ig.KrajDetail
  (@container, @geometryPreparer, @krajeMap, @downloadCache) ->
    @element = @container.append \div
      ..attr \class "kraj-detail"
    @mapContainer = @element.append \div
      ..attr \class \kraj-map-container
    @backbutton = ig.utils.backbutton @element
      ..on \click @~hide
    ig.Events @

  display: (krajId) ->
    features = null
    data = null
    @element.classed \active yes
    @geometryPreparer.getKraj krajId, (err, _features) ~>
      features := _features
      @displayWhenLoaded features, data if features and data
    @datasource = @downloadCache.getItem krajId
    processData = (_data) ~>
      data := _data
      @displayWhenLoaded features, data if features and data

    @datasource.get (err, _data) ~>
      processData _data
      @datasource.on \downloaded (_data) ~>
        data := _data
        if features and data
          results = resultsDecoder.decode data
          @krajMap.setResults results
          @setStranaId @stranaId


  hide: ->
    @element.classed \hiding yes
    @element.classed \showing no
    @krajMap.svg.style \transform @currentKrajMapTransform
    setTimeout do
      ~>
        @element.classed \active no
        @element.classed \hiding no
        @mapContainer.html ""
        @krajMap = null

      900
    @emit \hide
    @krajeMap.element.style \opacity 1

  displayWhenLoaded: (features, data) ->
    scale = @krajeMap.projection.scale! / features.projection.scale!
    testCoords = [features.bounds.0.0, features.bounds.1.1]
    localCoords = features.projection testCoords
    remoteCoords = @krajeMap.projection testCoords
    translation = [remoteCoords.0 - localCoords.0, remoteCoords.1 - localCoords.1]

    @krajMap = krajMap = new ig.KrajMap @mapContainer, features
      ..on \obec-mouseover (id, data) ~> @emit \obec-mouseover, id, data
      ..on \obec-mouseout (id, data) ~> @emit \obec-mouseout, id, data
    results = resultsDecoder.decode data
    krajMap.setResults results
    krajMap.displayWinners!
    @currentKrajMapTransform = "translate(#{translation.0}px, #{translation.1}px) scale(#scale)"
    krajMap.svg.style \transform "translate(#{translation.0}px, #{translation.1}px) scale(#scale)"
    <~ setTimeout _, 1
    @element.classed \showing yes
    <~ setTimeout _, 500
    @krajeMap.element.style \opacity 0
    overflow = features.height - 270
    if overflow < 0
      overflow = 0
    left = -1 * (features.width - 400) / 2
    # left = features.width
    krajMap.svg.style \transform "scale(1) translate(#{left}px, #{overflow * -1}px)"

  setStranaId: (@stranaId) ->
    if @stranaId
      @krajMap.displayStatStrana @stranaId
    else
      @krajMap.displayWinners!
