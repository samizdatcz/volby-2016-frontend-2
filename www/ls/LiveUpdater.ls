toThree = ->
  it .= toString!
  while it.length < 3
    it = "0" + it
  it

server = "https://interaktivni.rozhlas.cz/"
window.ig.LiveUpdater = class LiveUpdater
  (@downloadCache) ->
    @lastMessage = new Date!
    return
    try
      es = new EventSource "#server/sse"
      es.onmessage = (event) ~>
        @lastMessage = new Date!
        len = event.data.length
        data = for i in [0 til len]
          event.data.charCodeAt i
        for code in data
          switch code
          | 1 => @update "obce"
          | 2 => @update "senat"
          | 3 => @update "kraje"
          | 4, 5 => console?log? "Hello"
          | 6 => window.location.reload!
          | otherwise =>
              @update "CZ" + toThree code
          #   @update @getObecId code

  isOnline: ->
    return true
    t = new Date!getTime!
    t - @lastMessage.getTime! < 60_000

  update: (dataType) ->
    return unless dataType
    item = @downloadCache.items[dataType]
    item?invalidate!

  getObecId: (code) ->
    code -= 20
    if window.ig.suggester.suggestions
      obec = window.ig.suggester.suggestions[code]
      if obec
        return obec.id
    return null
