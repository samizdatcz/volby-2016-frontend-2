class ig.SenatBar1Kolo implements ig.utils.supplementalMixin
  (@parentElement, @senatKandidati, @senatData) ->
    @element = @parentElement.append \div
      ..attr \class \senat-bar-1kolo
    @heading = @element.append \h2
    @subHeading = @element.append \div
      ..attr \class \sub-heading
    list = @element.append \ol
    @people = list.selectAll \li .data [0 til 5] .enter!append \li
      ..append \span
        ..attr \class \counter
        ..html -> "#{it + 1}."
      ..append \img
      ..append \span
        ..attr \class \bar-label
      ..append \div
        ..attr \class \name-party
        ..append \h3 .attr \class \name
        ..append \h4 .attr \class \party
      # ..append \div
      #   ..attr \class \bar-container
      #   ..append \div
      #     ..attr \class \bar-area
      #     ..append \span
      #     ..attr \class \bar-label
    @drawSupplemental @subHeading
    @statusString = @subHeading.append \span
    @senatData.on \downloaded @~redraw

  draw: (obvod) ->
    @currentObvod = obvod
    @redraw! if @senatData.data

  redraw: ->
    cislo = @currentObvod.properties.VOL_OKR
    nazev = @currentObvod.properties.SIDLO
    data = @senatData.data
    obvod = data.obvody[cislo]
    @stats =
      okrsky_zprac: obvod.okrsky_spocteno
      okrsky_celkem: obvod.okrsky_celkem
      platne_hlasy: obvod.volilo
      zapsani_volici: obvod.volicu
    @element.classed \hidden obvod.okrsky_spocteno == 0
    {kandidati} = obvod
    kandidati.sort (a, b) -> b.hlasu - a.hlasu
    totalHlasu = 0
    for kandidat in kandidati
      totalHlasu += kandidat.hlasu
    totalHlasu = 1 if totalHlasu is 0
    for kandidat in kandidati
      kandidat.data = @senatKandidati.find cislo, kandidat.id
      kandidat.percent = kandidat.hlasu / totalHlasu

    @updateSupplemental!
    @statusString.html switch
      | @stats.okrsky_zprac == 0 => "Čekáme na první výsledky"
      | @stats.okrsky_zprac == @stats.okrsky_celkem => "Celkové výsledky"
      | otherwise => "Průběžné výsledky"
    @heading.html "#{nazev} <span>(obvod #{cislo})</span>"
    @people
      ..select \img .attr \src -> "https://samizdat.blob.core.windows.net/vizitky16/" + kandidati[it].data.photo
      ..select \h3 .html -> kandidati[it].data.name
      ..select \h4 .html -> kandidati[it].data.partyShort || kandidati[it].data.party
      # ..select \.bar-area .style \height -> "#{kandidati[it].percent}%"
      ..select \.bar-label .html -> "#{ig.utils.formatNumber kandidati[it].percent * 100, 2}%"
    # @senatData.get

  setFeatures: (@features) ->
