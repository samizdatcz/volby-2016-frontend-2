ig.utils = utils = {}


utils.offset = (element, side) ->
  top = 0
  left = 0
  do
    top += element.offsetTop
    left += element.offsetLeft
  while element = element.offsetParent
  {top, left}


utils.deminifyData = (minified) ->
  out = for row in minified.data
    row_out = {}
    for column, index in minified.columns
      row_out[column] = row[index]
    for column, indices of minified.indices
      row_out[column] = indices[row_out[column]]
    row_out
  out


utils.formatNumber = (input, decimalPoints = 0) ->
  input = parseFloat input
  if decimalPoints
    wholePart = Math.floor input
    decimalPart = Math.abs input % 1
    decimalPart = Math.round decimalPart * Math.pow 10, decimalPoints
    if decimalPart >= Math.pow 10, decimalPoints
      decimalPart -= Math.pow 10, decimalPoints
      wholePart += 1
    wholePart = insertThousandSeparator wholePart
    decimalPart = decimalPart.toString()
    while decimalPart.length < decimalPoints
      decimalPart = "0" + decimalPart
    "#{wholePart},#{decimalPart}"
  else
    wholePart = Math.round input
    insertThousandSeparator wholePart


insertThousandSeparator = (input, separator = ' ') ->
    price = Math.round(input).toString()
    out = []
    len = price.length
    for i in [0 til len]
      out.unshift price[len - i - 1]
      isLast = i is len - 1
      isThirdNumeral = 2 is i % 3
      if isThirdNumeral and not isLast
        out.unshift separator
    out.join ''

utils.divideToParts = (extent, breaks) ->
  for i in [0 til breaks]
    extent.0 + i * ((extent.1 - extent.0) / (breaks - 1))

utils.download = (url, cb) ->
  isJson = url.split "." .pop! == "json"
  if isJson
    d3.json url, cb
  else
    d3.text url, cb

utils.supplementalMixin =
  updateSupplemental: ->
    sectenoPerc = utils.percentage @stats.okrsky_zprac / @stats.okrsky_celkem
    if sectenoPerc == "100,0" and @stats.okrsky_celkem != @stats.okrsky_zprac
      sectenoPerc = "99,9"
    @sectenoValue.html "#{sectenoPerc}&nbsp;%"
    @sectenoFill.style \width "#{@stats.okrsky_zprac / @stats.okrsky_celkem * 100}%"
    if @stats.zapsani_volici
      @ucastValue.html   "#{utils.percentage @stats.platne_hlasy / @stats.zapsani_volici}&nbsp;%"
      @ucastFill.style \width "#{@stats.platne_hlasy / @stats.zapsani_volici * 100}%"
    else
      @ucastValue.html   "&ndash;"
      @ucastFill.style \width "0%"

  drawSupplemental: (parentElement = @element) ->
    @supplemental = parentElement.append \div
      ..attr \class \supplemental
    @secteno = @supplemental.append \div
      ..attr \class \secteno
      ..append \h3 .html "Sečteno"
      ..append \div
        ..attr \class \progress
        ..append \div
          ..attr \class \fill
      ..append \span
        ..attr \class \value
    @sectenoValue = @secteno.select "span.value"
    @sectenoFill = @secteno.select "div.fill"

    @ucast = @supplemental.append \div
      ..attr \class \ucast
      ..append \h3 .html "Účast"
      ..append \div
        ..attr \class \progress
        ..append \div
          ..attr \class \fill
      ..append \span
        ..attr \class \value
    @ucastValue = @ucast.select "span.value"
    @ucastFill = @ucast.select "div.fill"

utils.backbutton = (parent) ->
  parent.append \a
    ..attr \class \closebtn
    ..html '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" baseProfile="full" width="76" height="76" viewBox="0 0 76.00 76.00" enable-background="new 0 0 76.00 76.00" xml:space="preserve"><path fill="#000000" fill-opacity="1" stroke-width="0.2" stroke-linejoin="round" d="M 57,42L 57,34L 32.25,34L 42.25,24L 31.75,24L 17.75,38L 31.75,52L 42.25,52L 32.25,42L 57,42 Z "/></svg>'

utils.percentage = ->
  window.ig.utils.formatNumber it * 100, 1

barvaIterator = 140
barvyAssigned = {}
utils.resetStranyColors = ->
  barvyAssigned := {}
  barvaIterator := 140

utils.getStranaColor = (strana, fallback) ->
  barva = null
  id = null
  if typeof! strana is 'Object'
    if strana.id
      barva = window.ig.strany[that].barva
      id = strana.id
    else if strana.barva
      barva = that
  else if strana
    id = strana
    barva = window.ig.strany[that]?barva

  if barva and barva.length > 1
    return barva
  else
    if !id
      id = strana.id || strana.uid
    if fallback
      return fallback
    else
      if id and barvyAssigned[id]
        return barvyAssigned[id]
      else
        barvaIterator += 40
        barvaIterator %= 220
        if barvaIterator < 100
          barvaIterator := 100
        barva = "rgb(#barvaIterator,#barvaIterator,#barvaIterator)"
        if id != null
          barvyAssigned[id] = barva
        return barva

toDouble = utils.toDouble = ->
  it .= toString!
  it = "0#it" if it.length == 1
  it

utils.humanDateTime = ->
  "#{it.getDate!}. #{it.getMonth! + 1}. #{it.getFullYear!} #{toDouble it.getHours!}:#{toDouble it.getMinutes!}"
