senatObvody = [1 4 7 10 13 16 19 22 25 28 31 34 37 40 43 46 49 52 55 58 61 64 67 70 73 76 79]
subMaps =
  * name: "Praha"
    obvody: [17 20 21 25 26 27 22 19 24 23]
    topLeft: [14.3 50.2]
    labelPosition: [-42 17]
  * name: "Ostrava"
    obvody: [70 71 72]
    topLeft: [18.0 50.2]
    labelPosition: [0 71]
  * name: "Brno"
    obvody: [58 59 60]
    topLeft: [17 49.28]
    labelPosition: [-68 5]
  ...
class ig.SenatMap1Kolo implements ig.utils.supplementalMixin
  (@container, @senatKandidati, @senatData) ->
    @element = @container.append \div
      ..attr \class \mapka
    @svg = @element.append \svg
    ig.Events @
    @currentObvod = null
    window.addEventListener \resize @~resize
    @validityNotice = @element.append \div
      ..attr \class \validity-notice
    (err, data) <~ d3.json "#{ig.addrPrefix}/data/senat.topo.json"
    @topo = data
    @draw!
    @drawSupplemental!
    (err, data) <~ @senatData.get
    @senatData.on \downloaded @~update
    @update data if not err
    @emit \features-ready @features

  update: (newData) ->
    @stats =
      okrsky_zprac: newData.okrsky_spocteno
      okrsky_celkem: newData.okrsky_celkem
      platne_hlasy: newData.volilo
      zapsani_volici: newData.volicu

    @updateSupplemental!
    @updateValidityNotice!
    @stops.attr \offset ->
      obvod = newData.obvody[it]
      percentage = obvod.okrsky_spocteno / obvod.okrsky_celkem
      percentage *= 100
      "#{percentage}%"

  draw: ->
    options = features = @features = topojson.feature @topo, @topo.objects."data" .features
    featuresAssoc = []
    for feature in features
      feature.properties.isInactive = not (feature.properties.VOL_OKR in senatObvody)
      featuresAssoc[feature.properties.VOL_OKR] = feature
    options = features.filter -> !it.properties.isInactive
    subMapsWidth = 100
    nodeWidth = (@element.node!offsetWidth || 500) - subMapsWidth
    subMapsHeight = 70
    {width, height, projection} = ig.utils.geo.getFittingProjection features, {width: nodeWidth - 30}
    @fullWidth = width + subMapsWidth
    @fullHeight = height + subMapsHeight
    path = d3.geo.path!
      ..projection projection
    @svg
      ..attr \width @fullWidth
      ..attr \height @fullHeight
      ..html ""
    features.sort (a, b) -> b.properties.isInactive - a.properties.isInactive
    @svg.append \defs
      ..selectAll \linearGradient .data senatObvody .enter!append \linearGradient
        ..attr \id -> "gradient-#it"
        ..append \stop
          ..attr \offset \0%
          ..attr \stop-color \#999
        ..append \stop
          ..attr \offset \0%
          ..attr \stop-color \#e8e8e8
    @stops = @svg.selectAll \stop
    @paths = @svg.selectAll \path .data features .enter!append \path
      ..attr \d -> path it
      ..attr \class \obvod
      ..classed \is-inactive -> it.properties.isInactive
      ..classed \is-active -> !it.properties.isInactive
    for subMap in subMaps
      subMap.projection = d3.geo.mercator!
        ..scale projection.scale! * 4
        ..center subMap.topLeft
        ..translate [0 0]
      subMap.path = d3.geo.path!
        ..projection subMap.projection
      subMap.features = subMap.obvody.map -> featuresAssoc[it]
      subMap.features.sort (a, b) -> b.properties.isInactive - a.properties.isInactive
    @svg.append \g
      ..attr \class \sub-maps
      ..attr \transform "translate(#{@fullWidth - subMapsWidth}, 0)"
      ..selectAll \g .data subMaps .enter!append \g
        ..attr \class \sub-map
        ..attr \transform (it, i) -> "translate(0, #{height / 2 * i})"
        ..append \text
          ..text (.name)
          ..attr \transform -> "translate(#{it.labelPosition.join ','})"
        ..selectAll \path .data (.features) .enter!append \path
          ..attr \class \obvod
          ..attr \d (it, i, ii) -> subMaps[ii].path it
          ..classed \is-inactive -> it.properties.isInactive
          ..classed \is-active -> !it.properties.isInactive
    @obvody = @svg.selectAll \path.obvod.is-active
      ..attr \fill -> 'url(#gradient-' + it.properties.VOL_OKR + ')'
      ..on \mouseover @~onObvodSelected
      ..on \touchstart @~onObvodSelected


  updateValidityNotice: ->
    vysledkyType = switch
      | @stats.okrsky_zprac == 0 => "Čekáme na první výsledky"
      | @stats.okrsky_zprac == @stats.okrsky_celkem => "Celkové výsledky"
      | otherwise => "Průběžné výsledky"
    @validityNotice.html "Platné k #{ig.utils.humanDateTime window.ig.liveUpdater.lastMessage} | #{vysledkyType}"

  resize: ->
    return unless @fullWidth
    ratio = (@element.node!offsetWidth - 30) / @fullWidth
    @svg.style \transform "scale(#ratio)"
    @element.style \height "#{@fullHeight * ratio}px"

  onObvodSelected: (obvod) ->
    return if obvod.properties.isInactive
    @emit \obvod-mouseover obvod
    @currentObvod = obvod
    @obvody.classed \selected -> it is obvod
