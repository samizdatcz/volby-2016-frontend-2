class ig.Obce
  ->
    @suggestions = []
    @assoc = {}
    setTimeout @~downloadSuggestions, 500

  downloadSuggestions: (cb) ->
    return cb?! if @suggestions.length
    (err, text) <~ d3.text "https://interaktivni.rozhlas.cz/tools/suggester/obce-2016.tsv"
    text .= replace /\r/g ""
    [okresy, obce] = text.split "\n\t\t\t\n"
    okresy_assoc = {}
    okresy.split "\n"
      .map (.split "\t")
      .forEach ([kod, nazev]) -> okresy_assoc[kod] = {kod, nazev}
    @suggestions = for line in obce.split "\n"
      continue unless line.length > 2
      [id, okres_kod, nazev, senatKod] = line.split "\t"
      okres = okresy_assoc[okres_kod]
      id = parseInt id, 10
      nazevSearchable = nazev.toLowerCase!
      obec = {id, okres, nazev, nazevSearchable, senatKod}
      @assoc[id] = obec
      obec
    @suggestions.sort (a, b) -> a.id - b.id
    cb?!
