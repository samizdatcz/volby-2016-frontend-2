defaultKrajId = "CZ020"
ig.addrPrefix = "https://interaktivni.rozhlas.cz/data-r/volby-2016-frontend/www"
ig.addrPrefix = "." if window.location.hostname is \localhost
container = d3.select ig.containers.base
  # ..classed \kraje yes
  ..classed \senat yes
try
  if window.localStorage?smz_vlb_lastSuggestion
    preselected = JSON.parse that
    defaultKrajId = preselected.okres.kod.substr 0, 5
if container.node!getAttribute \data-region
  defaultKrajId = that.toUpperCase!
geometryPreparer = new ig.GeometryPreparer
downloadCache1Kolo = new ig.DownloadCache1Kolo
window.ig.downloadCache = downloadCache = new ig.DownloadCache!
window.ig.liveUpdater = new ig.LiveUpdater downloadCache
strany = ig.strany
container.append \div
  ..attr \class \tabs
  ..append \h1
    ..append \a
      ..attr \href \#
      ..on \click ->
        d3.event.preventDefault!
        container.classed \senat yes
        container.classed \senat1 no
        container.classed \kraje no
      ..html "Senátní volby 2. kolo"
    ..append \span .html " | "
    ..append \a
      ..html "1. kolo"
      ..attr \href \#
      ..on \click ->
        d3.event.preventDefault!
        container.classed \senat no
        container.classed \senat1 yes
        container.classed \kraje no
    ..append \span .html " | "
    ..append \a
      ..attr \href \#
      ..on \click ->
        d3.event.preventDefault!
        container.classed \senat no
        container.classed \senat1 no
        container.classed \kraje yes
      ..html "Krajské volby"

krajeContainer = container.append \div
  ..attr \class \kraje-container
mapsContainer = krajeContainer.append \div
  ..attr \class \maps-container
  ..append \div
    ..attr \class \navod
    ..html "Najetím myši zvolte volební kraj, kliknutím zobrazíte podrobnosti"
pekacContainer = krajeContainer.append \div
  ..attr \class \pekac-container
pekac = new ig.Pekac pekacContainer, strany, downloadCache1Kolo
  ..krajId = defaultKrajId
pekac.init!
currentKrajDetailId = no
obce = new ig.Obce
krajeMap = new ig.KrajeMap mapsContainer, downloadCache1Kolo
  ..on \kraj-mouseover ->
    if not currentKrajDetailId
      pekac.setKrajId it
  ..on \kraj-mouseout ->
    if not currentKrajDetailId
      pekac.setKrajId null
  ..on \kraj-click ->
    return if it == "CZ010"
    return if currentKrajDetailId
    currentKrajDetailId := it
    pekac.setKrajId it
    krajDetail.display it
pekac
  ..on \strana-mouseover ->
    if currentKrajDetailId
      krajDetail.setStranaId it
    else
      krajeMap.setStranaId it
  ..on \strana-mouseout ->
    if currentKrajDetailId
      krajDetail.setStranaId null
    else
      krajeMap.setStranaId null
krajDetail = new ig.KrajDetail mapsContainer, geometryPreparer, krajeMap, downloadCache1Kolo
  ..on \obec-mouseover (id, data) ~>
    pekac.setData data, obce.assoc[id]
  ..on \obec-mouseout ->
    pekac.setKrajId currentKrajDetailId
  ..on \hide ->
    currentKrajDetailId := null
    pekac.setKrajId null
let
  senatContainer = container.append \div
    ..attr \class "senat-container kolo1"
  senatKandidati = new ig.SenatKandidati
  senatData = downloadCache1Kolo.getItem "senat"
  senatData.download!
  senatMap = new ig.SenatMap1Kolo senatContainer, senatKandidati, senatData
  senatBar = new ig.SenatBar1Kolo senatContainer, senatKandidati, senatData
  senatMap.on \features-ready (features) ->
    assoc = {}
    for feature in features
      assoc[feature.properties.VOL_OKR] = feature

    senatBar.setFeatures features
    senatBar.draw assoc[1]
  senatMap.on \obvod-mouseover senatBar~draw

let
  senatContainer = container.append \div
    ..attr \class "senat-container kolo2"
  ig.senatKandidati = senatKandidati = new ig.SenatKandidati
  senatData = downloadCache.getItem "senat"
  senatData.download!
  senatMap = new ig.SenatMap senatContainer, senatKandidati, senatData
  senatBar = new ig.SenatBar senatContainer, senatKandidati, senatData
  senatMap.on \features-ready (features) ->
    assoc = {}
    for feature in features
      assoc[feature.properties.VOL_OKR] = feature

    senatBar.setFeatures features
    senatBar.draw assoc[1]
  senatMap.on \obvod-mouseover senatBar~draw

  new Tooltip!watchElements!

  window.ig.senat_obvody_meta = obvody_meta = {}
  for line in window.ig.data.senat_obvody.split "\n"
    [id, nazev] = line.split "\t"
    obvody_meta[id] = {nazev}

  senatDetailContainer = container.append \div
    ..attr \class \kosti-container
    # ..classed \active yes
  moreLinkIsActive = no
  moreLink = senatContainer.append \a
    ..html "<span>Zobrazit získané a ztracené mandáty</span> <div class='raquo'>›</div>"
    ..attr \class \more
    ..attr \href \#
    ..on \click ->
      if moreLinkIsActive
        moreLink.classed \active no
        senatDetailContainer.classed \active no
        moreLinkIsActive := no
        moreLink.select \span .html "Zobrazit získané a ztracené mandáty"
      else
        moreLink.classed \active yes
        moreLink.select \span .html "Skrýt získané a ztracené mandáty"
        senatDetailContainer.classed \active yes
        moreLinkIsActive := yes
  new window.ig.Pekacek senatDetailContainer, downloadCache, senatKandidati

  window.ig.senat = senat = new window.ig.SenatOverview senatDetailContainer, downloadCache, senatKandidati

