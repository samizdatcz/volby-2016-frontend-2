krajeHuman =
  CZ010: "Praha"
  CZ020: "Středočeský kraj"
  CZ031: "Jihočeský kraj"
  CZ032: "Plzeňský kraj"
  CZ041: "Karlovarský kraj"
  CZ042: "Ústecký kraj"
  CZ051: "Liberecký kraj"
  CZ052: "Královéhradecký kraj"
  CZ053: "Pardubický kraj"
  CZ063: "kraj Vysočina"
  CZ064: "Jihomoravský kraj"
  CZ071: "Olomoucký kraj"
  CZ072: "Zlínský kraj"
  CZ080: "Moravskoslezský kraj"

utils = window.ig.utils
window.ig.Pekac = class Pekac implements utils.supplementalMixin
  (@baseElement, @strany, @downloadCache, @krajId) ->
    ig.Events @
    @element = @baseElement.append \div
      ..attr \class \pekac
    @heading = @element.append \h2
      ..html "Průběžné výsledky krajských voleb"
    @subHeading = @element.append \div
      ..attr \class \sub-heading
    @updatedString = @subHeading.append \span
      ..attr \class \updated
    @bars = @element.append \div
      ..attr \class \bars
    @pager = @bars.append \div
      ..attr \class \pager
    @currentPage = 0
    @toDisplay = "mandaty"
    @subPekac = @element.append \span
      ..attr \class \sub-pekac
      ..append \span
        ..html "Výsledky jsou seřazeny podle počtu získaných mandátů"
    # @drawSwitcher!
    @drawSupplemental @subHeading
    @drawPaginator!

    @columnWidth = 60px
    @y = d3.scale.linear!
      ..domain [0 1]
      ..range [1 80]

  redraw: (obec) ->
    headingStr = ""
    if obec
      headingStr = "Obec #{obec.nazev}"
    else if @krajId
      headingStr = krajeHuman[@krajId]
    else
      headingStr = "Celkové výsledky"
    @heading.html headingStr
    @stats = @data.stats
    @hlasy = @stats.platne_hlasy

    @updateSupplemental!
    @updatedString.html "Platné k #{ig.utils.humanDateTime window.ig.liveUpdater.lastMessage}"
    @element.classed \is-obec !!obec

    @pager.selectAll \div.bar.active .data @data.strany, (.id)
      ..exit!remove!
      ..enter!append \div |> @initElms
    utils.resetStranyColors!
    @pager.selectAll \div.bar.active
      ..style \left ~> "#{@columnWidth * it.index}px"
      ..attr \data-tooltip ~>
        "<b>#{it.strana?nazev}</b><br>
        Získala #{utils.percentage it.hlasy / @hlasy}&nbsp;% hlasů a&nbsp;#{it.mandaty}&nbsp;zastupitelů"
      ..select \.barArea
        ..style \height ~> "#{@y it[@toDisplay]}%"
        ..select \.result-mandaty
          ..classed \empty (.mandaty is 0)
          ..select \span .html (.mandaty)
      ..select \.result
        ..html ~> "#{utils.percentage it.hlasy / @hlasy}&nbsp;%"

  init: ->
    @cacheItem = @downloadCache.getItem "kraje"
    (err, data) <~ @cacheItem.get
    @saveData data
    @cacheItem.on \downloaded @~saveData

  saveData: (data) ->
    return unless data
    @fullData = data
    @data = @getKraj data
    return unless @data
    @stats = @data.stats
    return if @currentObec
    @reorder!
    @redraw!

  setKrajId: (@krajId) ->
    @setData @getKraj @fullData

  setData: (@data, obec) ->
    if obec
      @toDisplay = "hlasy"
    else
      @toDisplay = "mandaty"
    @currentObec = obec
    unless @data
      return
    @stats = @data.stats
    @reorder!
    @redraw obec

  reorder: ->
    @data.strany.sort (a, b) ~>
      if @currentObec
        b.hlasy - a.hlasy
      else
        if b[@toDisplay] - a[@toDisplay]
          that
        else
          b.hlasy - a.hlasy
    @data.strany.forEach (d, i) ~>
      d.strana = @strany[d.id]
      d.index = i
    @y.domain [0 @data.strany.0[@toDisplay]] if @data.strany.length
    @nextPage.classed \disabled @data.strany.length == 0

  getKraj: (data) ->
    if @krajId
      for kraj in data
        if kraj.id is @krajId
          return kraj
    else
      return @sumKraje data

  sumKraje: (data) ->
    stranyAssoc = {}
    strany = []
    stats = {}
    for kraj in data
      for field, value of kraj.stats
        stats[field] ?= 0
        stats[field] += value if value
      for strana in kraj.strany
        uid = window.ig.strany[strana.id].uid
        if stranyAssoc[uid] is void
          stranyAssoc[uid] =
            id: strana.id
            hlasy: 0
            mandaty: 0
          strany.push stranyAssoc[uid]
        stranyAssoc[uid].hlasy += strana.hlasy
        stranyAssoc[uid].mandaty += strana.mandaty
    {stats, strany}


  initElms: ->
    it
      ..attr \class "bar active"
      ..on \mouseover ~> @emit \strana-mouseover it.id
      ..on \touchstart ~> @emit \strana-mouseover it.id
      ..on \mouseout ~> @emit \strana-mouseout it.id
      ..append \div
        ..attr \class \texts
        ..append \span
          ..attr \class \name
          ..html ~>
            if it.strana?zkratka
              that
            else
              it.strana?nazev
          ..attr \title -> it.strana?nazev
        ..append \span
          ..attr \class \result
      ..append \div
        ..attr \class \barArea
        ..append \div
          ..attr \class \barColor
          ..style \background-color ~>
            utils.getStranaColor it.strana
        ..append \span
          ..attr \class \result-mandaty
          ..append \svg
            ..attr \width 12
            ..attr \height 14
            ..append \g
              ..attr \transform "scale(0.25)"
              ..append \path
                ..attr \d "M39.287,41.955l-1.626-12.76c-0.556-4.375-4.278-7.61-8.688-7.61H16.985c-4.41,0-8.133,3.235-8.688,7.61L6.67,41.979 c-0.112,0.894,0.163,2.018,0.758,2.692c0.596,0.675,1.453,1.287,2.353,1.287h26.395c0.9,0,1.757-0.624,2.354-1.299 C39.125,43.982,39.4,42.85,39.287,41.955z"
              ..append \circle
                ..attr \cx 22.978
                ..attr \cy 9.33
                ..attr \r 9.33
          ..append \span

  movePage: (dir) ->
    @currentPage -= dir
    @pager.style \left "#{@currentPage * 100}%"
    if @currentPage == 0
      @prevPage.classed \disabled true
      <~ setTimeout _, 800
      @prevPage.classed \removed true
    else
      @prevPage.classed \removed false
      <~ setTimeout _, 800
      @prevPage.classed \disabled false

  drawPaginator: ->
    @nextPage = @element.append \a
      ..attr \class "paginator next"
      ..append \span .html "Další"
      ..on \click ~>
        d3.event.preventDefault!
        @movePage +1
    @prevPage = @element.append \a
      ..attr \class "paginator prev disabled removed"
      # ..append \span .html "Předchozí"
      ..on \click ~>
        d3.event.preventDefault!
        @movePage -1

  drawSwitcher: ->
    @element.append \div
      ..attr \class \switcher
      ..selectAll \label .data ["hlasy", "mandaty"] .enter!append \label
        ..append \input
          ..attr \type \radio
          ..attr \name \switcher
          ..attr \checked (d, i) -> if d == "hlasy" then "checked" else void
          ..on \click ~>
            @toDisplay = it
            @reorder!
            @redraw!
        ..append \span
          ..html ->
            | it is "hlasy" => "Řadit podle obdržených hlasů"
            | it is "mandaty" => "Řadit podle získaných mandátů"
